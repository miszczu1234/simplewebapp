use manageuser;

DROP TABLE user;
DROP TABLE city;

CREATE TABLE city(
	id int primary key auto_increment,
    name varchar(255) not null
);

CREATE TABLE user(
	id int primary key auto_increment,
    name varchar(255) not null,
    surname varchar(255) not null,
    id_city int,
    foreign key (id_city) references city(id)
);