package pl.kk.simplewebapp.html;

public enum HtmlAttributes {
	MESSAGE_SUCC("actionStatus"),
	MESSAGE_ERR("errorMessage"),
	EDIT_FORM("edit");
	
	private String attributeName;
	
	private HtmlAttributes(String attributeName) {
		this.attributeName= attributeName;
	}

	public String getAttributeName() {
		return attributeName;
	}
}
