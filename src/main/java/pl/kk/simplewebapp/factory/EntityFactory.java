package pl.kk.simplewebapp.factory;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import pl.kk.simplewebapp.bean.CityBean;
import pl.kk.simplewebapp.bean.UserBean;
import pl.kk.simplewebapp.entity.City;
import pl.kk.simplewebapp.entity.User;
import pl.kk.simplewebapp.service.CityService;

@Component
public class EntityFactory {

	@Autowired
	private CityService cityService;
	
	public User createUser(String name, String surname){
		User user= new User();
		user.setName(name);
		user.setSurname(surname);
		
		return user;
	}
	
	public User createUser(String name, String surname, String city){
		User user= createUser(name, surname);
		List<City> cities= cityService.getByName(city);
		if(!cities.isEmpty()){
			user.setCity(cities.get(0));
		}
		
		return user;
	}
	
	public User createUser(UserBean userBean){
		return createUser(userBean.getUsername(), userBean.getSurname(), userBean.getCity());
	}
	
	public City createCity(String cityname){
		City city= new City();
		city.setName(cityname);
		
		return city;
	}
	
	public City createCity(CityBean cityBean){
		return createCity(cityBean.getName());
	}
}
