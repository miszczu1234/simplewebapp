package pl.kk.simplewebapp.exception;

public class DataException extends Exception{
	
	public DataException() {
		super();
	}
	
	public DataException(String message){
		super(message);
	}
}
