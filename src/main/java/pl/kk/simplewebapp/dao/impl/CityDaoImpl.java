package pl.kk.simplewebapp.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import pl.kk.simplewebapp.dao.BaseDao;
import pl.kk.simplewebapp.dao.CityDao;
import pl.kk.simplewebapp.entity.City;

@Repository("cityDao")
public class CityDaoImpl extends BaseDao<City> implements CityDao{

	private static final String FIND_BY_NAME_NAMED_QUERY= "City.findByName";
	
	public CityDaoImpl() {
		super(City.class);
	}

	@Override
	public City findById(int id) {
		return super.findByPrimaryKey(id);
	}

	@Override
	public List<City> findAll() {
		return super.findAll();
	}

	@Override
	public List<City> findByName(String name) {
		Session session= getSession();
		Query query= session.getNamedQuery(FIND_BY_NAME_NAMED_QUERY)
						.setString("name", name);
		List<City> result= query.list();
		
		return (result==null) ? new ArrayList<City>() : result;
	}
}
