package pl.kk.simplewebapp.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import pl.kk.simplewebapp.dao.BaseDao;
import pl.kk.simplewebapp.dao.UserDao;
import pl.kk.simplewebapp.entity.User;

@Repository("userDao")
public class UserDaoImpl extends BaseDao<User> implements UserDao{

	public UserDaoImpl() {
		super(User.class);
	}

	@Override
	public User findById(int id) {
		return super.findByPrimaryKey(id);
	}

	@Override
	public List<User> findAll() {
		return super.findAll();
	}

	@Override
	public void persist(User user) {
		super.persist(user);
	}

}
