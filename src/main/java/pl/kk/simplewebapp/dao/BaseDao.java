package pl.kk.simplewebapp.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class BaseDao <T> {
	
	private final Class<T> persistentClass;
	
	@Autowired
	protected SessionFactory sessionFactory;
	
	public BaseDao(Class<T> persistentClass) {
		this.persistentClass= persistentClass;
	}
	
	protected Session getSession(){
		return sessionFactory.getCurrentSession();
	}
	
	public T findByPrimaryKey(Integer primaryKey){
		Session session= getSession();
		T resultEntity= session.get(persistentClass, primaryKey);
		
		return resultEntity;
	}
	
	public void persist(T entityObj){
		Session session= getSession();
		session.persist(entityObj);
	}
	
	public void delete(T entity){
		Session session= getSession();
		session.delete(entity);
	}
	
	public List<T> findAll(){
		Session session= getSession();
		return (List<T>)session.createCriteria(persistentClass).list();
	}
}
