package pl.kk.simplewebapp.dao;

import java.util.List;

import pl.kk.simplewebapp.entity.User;

public interface UserDao {
	
	User findById(int id);
	List<User> findAll(); 
	void persist(User user);
	
}
