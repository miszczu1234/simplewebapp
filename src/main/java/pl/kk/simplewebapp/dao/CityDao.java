package pl.kk.simplewebapp.dao;

import java.util.List;

import pl.kk.simplewebapp.entity.City;

public interface CityDao {
	
	City findById(int id);
	List<City> findAll();
	void persist(City city);
	List<City> findByName(String name);
	
}
