package pl.kk.simplewebapp.bean.factory;

import org.springframework.stereotype.Component;

import pl.kk.simplewebapp.bean.UserBean;
import pl.kk.simplewebapp.entity.City;
import pl.kk.simplewebapp.entity.User;

@Component
public class BeanFactory {
	public UserBean createUserBean(User user){
		UserBean userBean= new UserBean();
		userBean.setUsername(user.getName());
		userBean.setSurname(user.getSurname());
		userBean.setId(user.getId());
		City city= user.getCity();
		if(city!=null){
			userBean.setCity(user.getCity().getName());
		}
		
		return userBean;
	}
}
