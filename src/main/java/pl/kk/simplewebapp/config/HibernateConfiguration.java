package pl.kk.simplewebapp.config;

import java.util.Properties;

import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import pl.kk.simplewebapp.env.EnvVarsList;

@Configuration
@EnableTransactionManagement
@ComponentScan(basePackages={"pl.kk.simplewebapp.*"})
@PropertySource(value={"classpath:application.properties"})
public class HibernateConfiguration {
	
	@Autowired
	private Environment env;
	
	@Bean
	public DataSource getDataSource(){
		
		DriverManagerDataSource dataSource= new DriverManagerDataSource();
		dataSource.setDriverClassName(env.getRequiredProperty(EnvVarsList.DATABASE_DRIVER_CLASS_NAME.getPropertyName()));
		dataSource.setUrl(env.getRequiredProperty(EnvVarsList.DATABASE_URL.getPropertyName()));
		dataSource.setUsername(env.getProperty(EnvVarsList.DATABASE_USERNAME.getPropertyName()));
		dataSource.setPassword(env.getProperty(EnvVarsList.DATABASE_PASSWORD.getPropertyName()));
		
		return dataSource;
	}
	
	private Properties getHibernateProperties(){
		
		Properties properties= new Properties();
		properties.put(EnvVarsList.HIBERNATE_DIALECT.getPropertyName(), 
				env.getRequiredProperty(EnvVarsList.HIBERNATE_DIALECT.getPropertyName()));
		properties.put(EnvVarsList.HIBERNATE_SHOW_SQL.getPropertyName(), 
				env.getRequiredProperty(EnvVarsList.HIBERNATE_SHOW_SQL.getPropertyName()));
		properties.put(EnvVarsList.HIBERANTE_FORMAT_SQL.getPropertyName(),
				env.getRequiredProperty(EnvVarsList.HIBERANTE_FORMAT_SQL.getPropertyName()));
		
		return properties;
	}
	
	@Bean
	public LocalSessionFactoryBean sessionFactory(){
		
		LocalSessionFactoryBean sessionFactory= new LocalSessionFactoryBean();
		sessionFactory.setDataSource(getDataSource());
		sessionFactory.setPackagesToScan(new String[]{"pl.kk.simplewebapp.*"});
		sessionFactory.setHibernateProperties(getHibernateProperties());
		
		return sessionFactory;
	}
	
	@Bean
	@Autowired
	public HibernateTransactionManager transactionManager(SessionFactory sessionFactory){
		
		HibernateTransactionManager txManager= new HibernateTransactionManager();
		txManager.setSessionFactory(sessionFactory);
		
		return txManager;
	}
}
