package pl.kk.simplewebapp.config;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages= "pl.kk.simplewebapp.*")
public class WebConfiguration extends WebMvcConfigurerAdapter{

	private static final String VIEW_PREFIX= "/WEB-INF/views/";
	private static final String VIEW_SUFFIX= ".jsp";
	
	private static final String RESOURCE_HANDLER = "/resources/**";
	private static final String RESOURCE_LOCATION = "/resources/";
	
	private static final String LOCALIZE_BEAN_NAME= "messages";
	
	@Bean
	public ViewResolver viewResolver(){
		InternalResourceViewResolver viewResolver= new InternalResourceViewResolver();
		viewResolver.setViewClass(JstlView.class);
		viewResolver.setPrefix(VIEW_PREFIX);
		viewResolver.setSuffix(VIEW_SUFFIX);
		
		return viewResolver;
	}
	
	@Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
            registry.addResourceHandler(RESOURCE_HANDLER)
                    .addResourceLocations(RESOURCE_LOCATION);
    }
	
	@Bean
	public MessageSource messageSource(){
		
		ResourceBundleMessageSource localizeSrc= new ResourceBundleMessageSource();
		localizeSrc.setBasename(LOCALIZE_BEAN_NAME);
		
		return localizeSrc;
	}
}
