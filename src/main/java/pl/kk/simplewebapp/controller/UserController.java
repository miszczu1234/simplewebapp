package pl.kk.simplewebapp.controller;

import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import pl.kk.simplewebapp.bean.UserBean;
import pl.kk.simplewebapp.bean.factory.BeanFactory;
import pl.kk.simplewebapp.entity.City;
import pl.kk.simplewebapp.entity.User;
import pl.kk.simplewebapp.exception.DataException;
import pl.kk.simplewebapp.factory.EntityFactory;
import pl.kk.simplewebapp.html.HtmlAttributes;
import pl.kk.simplewebapp.service.CityService;
import pl.kk.simplewebapp.service.UserService;

@Controller
@RequestMapping("/user")
public class UserController {
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private CityService cityService;
	
	@Autowired
	private EntityFactory entityFactory;
	
	@Autowired
	private BeanFactory beanFactory;
	
	@Autowired
	private MessageSource messageSource;
	
	@RequestMapping(value="/list", method= RequestMethod.GET)
	public String listUsersGet(ModelMap model){
		
		List<User> users= userService.getAll();
		model.addAttribute("users", users);
		List<City> cities= cityService.getAll();
		model.addAttribute("cities", cities);
		
		return "/user/users";
	}
	
	@RequestMapping(value="/save")
	public String userSavePost(@ModelAttribute("userBean")@Validated UserBean userBean, BindingResult bindingResult,
			RedirectAttributes redirectAttrs){
		
		if(bindingResult.hasErrors()){
			redirectAttrs.addFlashAttribute(HtmlAttributes.MESSAGE_ERR.getAttributeName(),
					messageSource.getMessage("error.user.add", null, Locale.getDefault()));
			redirectAttrs.addFlashAttribute("org.springframework.validation.BindingResult.userBean", bindingResult);
			redirectAttrs.addFlashAttribute("userBean", userBean);
			return "redirect:/user/list";
		}
		
		User user= entityFactory.createUser(userBean);
		userService.save(user);
		
		redirectAttrs.addFlashAttribute(HtmlAttributes.MESSAGE_SUCC.getAttributeName(), 
				messageSource.getMessage("success.user.add", null, Locale.getDefault()));
		
		return "redirect:/user/list";
	}
	
	@RequestMapping(value="/edit/{userId}", method=RequestMethod.GET)
	public String userEditGet(RedirectAttributes redirectAttrs, @PathVariable("userId")Integer userId){
		
		User user= userService.getById(userId);
		UserBean userBean= beanFactory.createUserBean(user);
		redirectAttrs.addFlashAttribute(HtmlAttributes.EDIT_FORM.getAttributeName(), true);
		redirectAttrs.addFlashAttribute("userBean", userBean);
		
		return "redirect:/user/list";
	}
	
	@RequestMapping(value="/edit", method=RequestMethod.POST)
	public String userEditPost(@ModelAttribute("userBean")@Validated UserBean userBean, BindingResult bindingResult,
			RedirectAttributes redirectAttrs){
		
		if(bindingResult.hasErrors()){
			redirectAttrs.addFlashAttribute(HtmlAttributes.MESSAGE_ERR.getAttributeName(),
					messageSource.getMessage("error.user.edit", null, Locale.getDefault()));
			redirectAttrs.addFlashAttribute("org.springframework.validation.BindingResult.userBean", bindingResult);
			redirectAttrs.addFlashAttribute("userBean", userBean);
			redirectAttrs.addFlashAttribute(HtmlAttributes.EDIT_FORM.getAttributeName(), true);
			return "redirect:/user/list";
		}
		
		try {
			userService.update(userBean);
		} catch (DataException e) {
			redirectAttrs.addFlashAttribute(HtmlAttributes.MESSAGE_ERR.getAttributeName(),
					e.getMessage());
		}
		
		redirectAttrs.addFlashAttribute(HtmlAttributes.MESSAGE_SUCC.getAttributeName(), 
				messageSource.getMessage("success.user.edit", null, Locale.getDefault()));
		
		return "redirect:/user/list";
	}
}
