package pl.kk.simplewebapp.controller;

import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import pl.kk.simplewebapp.bean.CityBean;
import pl.kk.simplewebapp.entity.City;
import pl.kk.simplewebapp.html.HtmlAttributes;
import pl.kk.simplewebapp.service.CityService;

@Controller
@RequestMapping("/city")
public class CityController {
	
	@Autowired
	private CityService cityService;
	
	@Autowired
	private MessageSource messageSource;
	
	@RequestMapping("/list")
	public String listCitiesGet(ModelMap model){
		List<City> cities= cityService.getAll();
		model.addAttribute("cities", cities);
		
		return "/city/cities";
	}
	
	@RequestMapping(value="/save", method=RequestMethod.POST)
	public String saveCityPost(@ModelAttribute("cityBean")@Validated CityBean cityBean, BindingResult bindingResult,
			RedirectAttributes redirectAttrs){
		
		if(bindingResult.hasErrors()){
			redirectAttrs.addFlashAttribute(HtmlAttributes.MESSAGE_ERR.getAttributeName(), 
					messageSource.getMessage("error.city.add", null, Locale.getDefault()));
			redirectAttrs.addFlashAttribute("org.springframework.validation.BindingResult.cityBean", bindingResult);
			redirectAttrs.addFlashAttribute("cityBean", cityBean);
			return "redirect:/city/list";
		}
		
		City city= new City();
		city.setName(cityBean.getName());
		cityService.save(city);
		
		redirectAttrs.addFlashAttribute(HtmlAttributes.MESSAGE_SUCC.getAttributeName(), 
				messageSource.getMessage("success.city.add", null, Locale.getDefault()));
		
		return "redirect:/city/list";
	}
	
}
