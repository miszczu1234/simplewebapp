package pl.kk.simplewebapp.env;

public enum EnvVarsList {
	
	DATABASE_URL("database.url"),
	DATABASE_USERNAME("database.username"),
	DATABASE_PASSWORD("database.password"),
	DATABASE_DRIVER_CLASS_NAME("database.driverClassName"),
	
	HIBERNATE_DIALECT("hibernate.dialect"),
	HIBERNATE_SHOW_SQL("hibernate.show_sql"),
	HIBERANTE_FORMAT_SQL("hibernate.format_sql");
	
	private String propertyName;
	
	private EnvVarsList(String propertyName) {
		this.propertyName= propertyName;
	}

	public String getPropertyName() {
		return propertyName;
	}
}
