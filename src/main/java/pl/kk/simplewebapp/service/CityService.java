package pl.kk.simplewebapp.service;

import java.util.List;

import pl.kk.simplewebapp.entity.City;

public interface CityService {
	
	public void save(City city);
	public List<City> getAll();
	public City getById(Integer id);
	public List<City> getByName(String name);
}
