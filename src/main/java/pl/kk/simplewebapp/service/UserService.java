package pl.kk.simplewebapp.service;

import java.util.List;

import pl.kk.simplewebapp.bean.UserBean;
import pl.kk.simplewebapp.entity.User;
import pl.kk.simplewebapp.exception.DataException;

public interface UserService {
	
	void save(User user);
	List<User> getAll();
	User getById(Integer id);
	void update(UserBean user) throws DataException;
	
}
