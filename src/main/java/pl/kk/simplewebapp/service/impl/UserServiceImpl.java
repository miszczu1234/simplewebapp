package pl.kk.simplewebapp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pl.kk.simplewebapp.bean.UserBean;
import pl.kk.simplewebapp.dao.UserDao;
import pl.kk.simplewebapp.entity.City;
import pl.kk.simplewebapp.entity.User;
import pl.kk.simplewebapp.exception.DataException;
import pl.kk.simplewebapp.service.CityService;
import pl.kk.simplewebapp.service.UserService;

@Service("userService")
@Transactional
public class UserServiceImpl implements UserService{
	
	@Autowired
	@Qualifier("userDao")
	private UserDao userDao;

	@Autowired
	private CityService cityService;
	
	@Override
	public void save(User user) {
		userDao.persist(user);
	}

	@Override
	public List<User> getAll() {
		return userDao.findAll();
	}

	@Override
	public User getById(Integer id) {
		return userDao.findById(id);
	}

	@Override
	public void update(UserBean userBean) throws DataException {
		User user= userDao.findById(userBean.getId());
		if(user==null){
			throw new DataException(String.format("User with given id '%d' doesn't exsist", userBean.getId()));
		}
		user.setName(userBean.getUsername());
		user.setSurname(userBean.getSurname());
		List<City> city= cityService.getByName(userBean.getCity());
		if(!city.isEmpty()){
			user.setCity(city.get(0));
		}
	}
}
