package pl.kk.simplewebapp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pl.kk.simplewebapp.dao.CityDao;
import pl.kk.simplewebapp.entity.City;
import pl.kk.simplewebapp.service.CityService;

@Service("cityService")
@Transactional
public class CityServiceImpl implements CityService{

	@Autowired
	private CityDao cityDao;
	
	@Override
	public void save(City city) {
		cityDao.persist(city);
	}

	@Override
	public List<City> getAll() {
		return cityDao.findAll();
	}

	@Override
	public City getById(Integer id) {
		return cityDao.findById(id);
	}

	@Override
	public List<City> getByName(String name) {
		return cityDao.findByName(name);
	}

}
