package pl.kk.simplewebapp.app;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import pl.kk.simplewebapp.config.WebConfiguration;

public class App extends AbstractAnnotationConfigDispatcherServletInitializer{

	private static final String SERVLET_MAPPING= "/";
	
	@Override
	public Class<?>[] getRootConfigClasses(){
		return new Class[]{WebConfiguration.class};
	}

	@Override
	protected Class<?>[] getServletConfigClasses() {
		return null;
	}

	@Override
	protected String[] getServletMappings() {
		return new String[]{SERVLET_MAPPING};
	}
	
}
