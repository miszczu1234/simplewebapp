<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/templates"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">


<t:pagetemplate>
	<jsp:attribute name="content">
		<h1>Manage Customers Data Application</h1>
		<p>Hello, this is very simple application to manage customers data. Please select option from menu above.</p>
	</jsp:attribute>
</t:pagetemplate>