<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/templates"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<jsp:useBean id="userBean" scope="request" class="pl.kk.simplewebapp.bean.UserBean" />

<t:pagetemplate>
	<jsp:attribute name="content">
		<h1>Customers</h1>
		
		<c:if test="${empty edit}">
			<h3>Register new customer</h3>
			<spring:url value="/user/save" var="userFormActionUrl"/>
		</c:if>
		<c:if test="${not empty edit}">
			<h3>Edit customer</h3>
			<spring:url value="/user/edit" var="userFormActionUrl"/>
		</c:if>
		
		<t:validatorAction />
		
		<form:form method="post" commandName="userBean" action="${userFormActionUrl}">
			<form:input type="hidden" path="id" />
			<div class="form-group">
				<label>Customer Name</label>
				<form:input path="username" />
				<div><label class="alert-danger"><form:errors path="username"/></label></div>
			</div>
			<div class="form-group">
				<label>Customer surname</label>
				<form:input path="surname" />
				<div><label class="alert-danger"><form:errors path="surname"/></label></div>
			</div>
			<div class="form-group">
				<label>Customer city</label>
				<form:select path="city">
					<c:forEach var="cityEl" items="${cities}">
						<form:option value="${cityEl.name}" />
					</c:forEach>
				</form:select>
				<div><label class="alert-danger"><form:errors path="city"/></label></div>
			</div>
			<input class="btn btn-default" type="submit" value="Save"/>
		</form:form>
		<br />
		<table id="user-data-table" class="table">
			<thead>
			<tr>
				<th>Id</th>
				<th>Name</th>
				<th>Surname</th>
				<th>City</th>
				<th>Options</th>
			</tr>
			</thead>
			<c:forEach var="user" items="${users}">
				<tr>
					<td><c:out value="${user.id}" /></td>
					<td><c:out value="${user.name}" /></td>
					<td><c:out value="${user.surname}" /></td>
					<td><c:out value="${user.city.name}" /></td>
					<spring:url value="/user/edit" var="userEditUrl" />
					<td>
						<a href="${userEditUrl}/${user.id}" class="btn btn-success btn-lg">
							<span class="glyphicon glyphicon-pencil"></span> Edit
						</a>
					</td>
				</tr>
			</c:forEach>
		</table>
	</jsp:attribute>
</t:pagetemplate>