<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/templates"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; utf-8">
</head>

<jsp:useBean id="cityBean" scope="request" class="pl.kk.simplewebapp.bean.CityBean" />

<t:pagetemplate>
	<jsp:attribute name="content">
		<h1>Cities</h1>
		<h3>Register new city</h3>
		<t:validatorAction />
		<spring:url value="/city/save" var="saveUserUrl"/>
		<form:form method="post" modelAttribute="cityBean" action="${saveUserUrl}">
			<div class="form-group">
				<label>City Name</label>
				<form:input id="name" path="name" />
				<div><label class="alert-danger"><form:errors path="name"/></label></div>
			</div>
			<input class="btn btn-default" type="submit" value="Save"/>
		</form:form>
		<br />
		<table class="table">
			<thead>
			<tr>
				<th>Id</th>
				<th>Name</th>
			</tr>
			</thead>
			<tbody>
			<c:forEach var="city" items="${cities}">
				<tr>
					<td><c:out value="${city.id}" /></td>
					<td><c:out value="${city.name}" /></td>
				</tr>
			</c:forEach>
			</tbody>
		</table>
	</jsp:attribute>
</t:pagetemplate>

</html>