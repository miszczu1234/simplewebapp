<%@tag language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@attribute name="content" fragment="true"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/templates"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; utf-8">
        <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
        <title>Manage Users</title>
    </head>
<body>	
	<div class="container">
		<div class="jumbotron">
    		<h1>Manage Customers Application</h1>      
    		<p>Manage customers data</p>
  		</div>
  		<div>
  			<div class="container-fluid">
            	<div>
  					<ul class="nav navbar-nav">
  						<spring:url value="/" var="homeUrl"/>
            			<li><a href="${homeUrl}">Home</a></li>
            			<spring:url value="/user/list" var="userUrl"/>
               			<li><a href="${userUrl}">Customers</a></li>
               			<spring:url value="/city/list" var="cityUrl"/>
               			<li><a href="${cityUrl}">Cities</a></li>
            		</ul>
            	</div>
            </div>
  		</div>
  		<div id="id-content" class="content">
        	<jsp:invoke fragment="content" />
        </div>
        <div id="id-footer-content" class="col-md-12 well well-lg">
        	@Footer
		</div>
	</div>
	</body>
</html>
